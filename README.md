## COBalD/TARDIS all-in-one project

This project regroups three projects meant to be used together to provide a fully functionnal COBalD/TARDIS playground pool running inside Virtual Machines. As such, it is not meant to be secure nor performant !

The documentation for this project can be found here :

[https://cobaldtardis-all-in-one.readthedocs.io/en/latest/vagrant-cluster.html](https://cobaldtardis-all-in-one.readthedocs.io/en/latest/vagrant-cluster.html)

Build dependencies:

 - docutils >= 0.17
 - myst-parser
 - sphinx-rtd-theme
