.. sphinx-tutorial documentation master file, created by
   sphinx-quickstart on Thu Feb  9 09:44:00 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

COBalD/TARDIS all-in-one playground setup 
===========================================

This is the documentation to three projects, :doc:`vagrant-cluster`, :doc:`compose-project` and :doc:`monitoring` that can be used together to provide a fully functional `COBalD`_/`TARDIS`_ playground setup.

.. _COBalD: https://cobald.readthedocs.io/en/stable/
.. _TARDIS: https://cobald-tardis.readthedocs.io/en/latest/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   vagrant-cluster
   compose-project
   monitoring

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
