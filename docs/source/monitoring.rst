Monitoring
==========

--------
Overview
--------

The monitoring project provides basic monitoring to a COBalD/TARDIS setup, using `Telegraf`_, `Grafana`_ and either `Influx`_ or `Prometheus`_, showing various information about your COBalD/TARDIS pool:

.. _Telegraf: https://docs.influxdata.com/telegraf/v1.25/

.. _Grafana: https://grafana.com/docs/grafana/latest/?src=ggl-s&mdm=cpc&cnt=118483912276&camp=b-grafana-exac-emea&trm=grafana

.. _Influx: https://www.influxdata.com/

.. _Prometheus: https://prometheus.io/

-  Drone states, Age and Last Update
-  CPU, Memory and Disk usage,
-  Supply, Demand, Allocation and Utilization. 

------------
Dependencies
------------

- python3
- docker
- docker-compose v2
- jinja2

------------
Installation
------------

To install this project, you just have to pull it from the `GitLab repository`_.
Make sure to be inside the `plugin` directory of the compose project (cobaldtardis-compose/build/plugin/) before pulling.

.. _Gitlab repository: https://gitlab.com/aniclot/cobaldtardis-monitoring-plugin/-/tree/main

-----
Setup
-----

Everything is already set for this project to work together with the other two projects.
Nonetheless, if you want to tweak something, all you have to do is access the .env file and change the variables to your liking.
You should refer to the `README file`_ on GitLab to make sure your modifications will work correctly.
In particular, the project's default monitoring tool is Influx. If you would like to use Prometheus, just change the "datasource" variable to "prometheus".

.. _README file: https://gitlab.com/aniclot/cobaldtardis-monitoring-plugin/-/blob/main/README.md

--------
Start up
--------

You have nothing to do to start this project since it is started automatically as a plugin when the compose project is started. 

----------
Monitoring
----------

This project provides monitoring via the Grafana Web UI and comes in handy with a set of pre-created dashboards. 
Two dashboards show respectively the Allocation & Utilization and the Supply & Demand of the COBalD/TARDIS pool.
One shows a table containing the state of the drones, their age, CPU, Memory and Disk usage, amd the time since their last state update.
The last one provides gage showing the CPU, Memory and Disk usage of the whole pool.

These dashboards can me modified from the Grafana UI if you wish to have them presented in a different way or showing additional information. 
