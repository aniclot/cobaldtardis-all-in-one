Compose Project
===============

--------
Overview
--------

The compose project provides a fully functionnal `COBald/TARDIS`_ instance meant to be run inside the vagrant cluster.
It uses services held inside `Docker containers`_, `HTCondor`_ as a batch system agent and `Singularity containers`_ as drones.

.. _COBalD/TARDIS: https://cobald-tardis.readthedocs.io/en/latest/

.. _Docker containers: https://docs.docker.com/

.. _HTCondor: https://htcondor.org/
 
.. _Singularity: https://docs.sylabs.io/guides/3.5/user-guide/introduction.html

------------
Dependencies
------------

- python3.8 or newer
- docker
- docker-compose v2
- jinja2 

------------
Installation
------------

To install this project, you just have to pull it from this `GitLab repository`_.
Make sure that the repository is pulled in the same directory as the Vagrantfile (i.e in vagrant-cluster/)

.. _GitLab repository: https://gitlab.com/Kleinemuehl/cobaldtardis-compose

-----
Setup
-----

This project is already set to work together with the vagrant-cluster and the monitoring project. 

-------
Startup 
-------

To start this project within the vagrant cluster, ssh inside the submit node, cd to this project, execute the configuration and build scripts, then start the docker containers.
To jump into the submit node, do:

.. code-block:: console

    $ cd vagrant-cluster
    $ vagrant up
    $ vagrant ssh sub
    
Now that you are in the submit node, you can start the project :

.. code-block:: console

    $ cd /vagrant/cobaldtardis-compose/
    $ ./configure.sh
    $ ./build.sh
    $ docker compose [--profile pool] up -d

Then, in order to submit jobs to HTCondor:

.. code-block:: console

    $ docker exec -it cobaldtardis-cobald-1 /bin/bash
    $ condor_submit your_job_file.sub
