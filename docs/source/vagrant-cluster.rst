Vagrant Cluster
===============

--------
Overview
--------

The vagrant-cluster project provides a cluster of virtual machines using `Vagrant`_, containing a variable number of virtual machines providing a `Slurm`_ pool:

.. _Vagrant: https://www.vagrantup.com/
.. _Slurm: https://slurm.schedmd.com/documentation.html

- One Controller (called ``hive``)
- One submit node (called ``sub``)
- ``n`` worker nodes (``wn0...``)

The machines will share a `BeeGFS`_ filesystem and CVMFS can be optionally installed as well.

.. _BeeGFS: https://www.beegfs.io/

Refer to the `GitLab repository`_ of this project.

.. _GitLab repository: https://gitlab.com/Kleinemuehl/vagrant-cluster/-/tree/master/

------
README
------

.. include:: ../../vagrant-cluster/Readme.md
    :start-line: 1
    :parser: myst_parser.sphinx_


